ARG CONTAINER_PROXY
FROM ${CONTAINER_PROXY}python:3.9-alpine

ARG CONTAINER_IMAGE
ENV CONTAINER_IMAGE="$CONTAINER_IMAGE"

# test platform support
ARG CONTAINER_DOCKER_MACHINE_ARCH
RUN uname -a \
    && set -e \
    && MACHINE="$(uname -m)" \
    && if [ -n "$CONTAINER_DOCKER_MACHINE_ARCH" ] ; then test "$MACHINE" = "$CONTAINER_DOCKER_MACHINE_ARCH" ; fi

# hadolint ignore=DL3018
RUN apk add --no-cache bash \
    && echo "image: $CONTAINER_IMAGE" \
    && echo "proxy: $CONTAINER_PROXY"

ARG PIP_INDEX_URL

# hadolint ignore=DL3013
RUN pip3 install --no-cache-dir --index-url "$PIP_INDEX_URL" click
