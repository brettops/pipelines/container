# Container Pipeline

<!-- BADGIE TIME -->

[![brettops pipeline](https://img.shields.io/badge/brettops-pipeline-209cdf?labelColor=162d50)](https://brettops.io)
[![pipeline status](https://gitlab.com/brettops/pipelines/container/badges/main/pipeline.svg)](https://gitlab.com/brettops/pipelines/container/-/commits/main)
[![pre-commit](https://img.shields.io/badge/pre--commit-enabled-brightgreen?logo=pre-commit&logoColor=white)](https://github.com/pre-commit/pre-commit)

<!-- END BADGIE TIME -->

Build, lint, and publish OCI container images in CI.

[View the documentation](https://brettops.gitlab.io/pipelines/container/)

## About

### Building

Two build strategies are defined:

- **Docker-in-Docker**\* - Build on a runner with the Docker socket mounted.

- **Kaniko** - Build using Google's
  [Kaniko](https://github.com/GoogleContainerTools/kaniko) tool.

\*_planned_

For the most common use cases, the Kaniko pipeline is recommended, as it has
minimal requirements for use.

### Linting

A [hadolint](https://github.com/hadolint/hadolint) job is defined for
Dockerfile validation.

### Versioning

Two tags are produced for every pipeline that runs:

- `${CONTAINER_VERSION}`

- `${CONTAINER_VERSION}-${CI_COMMIT_SHORT_SHA}`

In addition, the default branch also publishes the `latest` tag.

It is recommended to garbage collect old tags.

## Usage

### Project Setup

A Dockerfile in the project root is the minimum requirement to use this
pipeline.
