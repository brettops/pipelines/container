# Image tag names

```
${CONTAINER_REGISTRY}${CONTAINER_NAME_SUFFIX}:
```

The following variables are available to customize the generated image tag
names:

- [CONTAINER_IMAGE](../variables.md#container_image)

- [CONTAINER_NAME](../variables.md#container_name)

- [CONTAINER_NAME_SUFFIX](../variables.md#container_name_suffix)

- [CONTAINER_VERSION](../variables.md#container_version)

- [CONTAINER_VERSION_SUFFIX](../variables.md#container_version_suffix)
