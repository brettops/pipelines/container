# Use a Docker Hub proxy

Docker Hub [rate limits container image
pulls](https://docs.docker.com/docker-hub/download-rate-limit/). These limits
are easily hit in a CI environment, so it is recommended to enable a container
proxy.

GitLab's [Dependency Proxy](https://docs.gitlab.com/ee/user/packages/dependency_proxy/)
feature allows you to proxy Docker Hub containers through GitLab. However,
rather than use these values directly, use the
[CONTAINER_PROXY](../variables.md#container_proxy) variable to make it easy to change
proxy location in the future without updating every repository.

When this pipeline is included, CONTAINER_PROXY is set by default to the
following value:

```yaml
variables:
  CONTAINER_PROXY: "${CI_DEPENDENCY_PROXY_DIRECT_GROUP_IMAGE_PREFIX}/"
```

Note the trailing slash (`/`)! This allows projects to continue to function if
CONTAINER_PROXY is not configured.

It is recommended to configure CONTAINER_PROXY at the group level, so that CI
jobs can use proxied containers. Here is an example using the
[image](https://docs.gitlab.com/ee/ci/yaml/#image) keyword:

```yaml
image: "${CONTAINER_PROXY}alpine:latest"
```

This pipeline will automatically add CONTAINER_PROXY as a `--build-arg`, so you
can proxy `FROM` images in Dockerfiles as well:

```dockerfile
ARG CONTAINER_PROXY
FROM ${CONTAINER_PROXY}node:lts-bullseye
```
