# Pass build options

!!! caution "This page is a stub. You can help by expanding it."

The [CONTAINER_BUILD_OPTS](../variables.md#container_build_opts) variable may
be used to pass extra arguments to the container build command.

Docker and Kaniko sometimes have different names for the same option.

- For Docker jobs, see <https://docs.docker.com/engine/reference/commandline/buildx_build/>.

- For Kaniko jobs, see <https://github.com/GoogleContainerTools/kaniko#additional-flags>.

## `--build-arg`

`--build-arg` is the same for Docker and Kaniko.

```yaml
container-kaniko:
  variables:
    CONTAINER_BUILD_OPTS: >-
      --destination ${CONTAINER_NAME}:3.4.4-${CONTAINER_VERSION}
      --destination ${CONTAINER_NAME}:3.4-${CONTAINER_VERSION}
      --destination ${CONTAINER_NAME}:3-${CONTAINER_VERSION}
```

```yaml
container-kaniko:
  variables:
    CONTAINER_BUILD_OPTS: >-
      --build-arg PIP_INDEX_URL=$PYTHON_PYPI_DOWNLOAD_URL
      --platform linux/arm/v6
```

## `--tag` (docker) / `--destination` (kaniko)

TBD.
