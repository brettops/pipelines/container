# Container Pipeline

[![pipeline status](https://gitlab.com/brettops/pipelines/container/badges/main/pipeline.svg)](https://gitlab.com/brettops/pipelines/container/-/commits/main)
[![pre-commit](https://img.shields.io/badge/pre--commit-enabled-brightgreen?logo=pre-commit&logoColor=white)](https://github.com/pre-commit/pre-commit)

Build, lint, and publish OCI container images in CI.
