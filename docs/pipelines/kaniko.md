# `kaniko` pipeline

This pipeline builds containers via
[Kaniko](https://github.com/GoogleContainerTools/kaniko). Kaniko lacks many
features of Docker, but is highly portable across runner executors as it
can run without root access.

## Usage

Include the pipeline in the `.gitlab-ci.yml` file:

```yaml
include:
  - project: brettops/pipelines/container
    file: kaniko.yml
```
