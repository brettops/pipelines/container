# Custom pipeline

The following snippet shows how a custom pipeline might be defined:

```yaml
stages:
  - test
  - build
  - deploy

workflow:
  rules:
    - if: $CI_PIPELINE_SOURCE == "push" && $CI_OPEN_MERGE_REQUESTS
      when: never
    - when: always

include:
  - project: brettops/pipelines/container
    file: .include.yml

container-hadolint:
  extends: .container-hadolint

container-docker:
  extends: .container-docker

container-kaniko:
  extends: .container-kaniko
```
