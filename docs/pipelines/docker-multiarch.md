# `docker-multiarch` pipeline

This pipeline builds full multi-arch Docker containers. It builds them in
separate jobs for parallelism and then recombines them in a later manifest
creation job.

## Usage

Include the pipeline in the `.gitlab-ci.yml` file:

```yaml
include:
  - project: brettops/pipelines/container
    file: docker-multiarch.yml
```

Optionally, define architectures to build in a space-separated list. Defaults
to all available:

```yaml
variables:
  CONTAINER_MULTIARCH_PLATFORMS: amd64 arm32v6 arm32v7 arm64v8 i386
```

For a list of supported values, see
[CONTAINER_DOCKER_ARCH](../variables.md#container_docker_arch).
