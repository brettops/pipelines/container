# `docker` pipeline

This pipeline is a minimal container build with Docker + BuildKit. It's useful
for when you need the extra features of BuildKit but has a slower startup time.

## Usage

Include the pipeline in the `.gitlab-ci.yml` file:

```yaml
include:
  - project: brettops/pipelines/container
    file: docker.yml
```
